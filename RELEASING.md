# How to release new weather-app version to OpenStore

__Note: the instructions here are for manual release to Open Store only. For releasing to all other platforms please refer to [README.md](https://gitlab.com/ubports/development/apps/lomiri-weather-app/-/blob/main/README.md).__

- make sure all changes are merged or pushed to `main` branch
- wait for pipeline to build successfully
- update version number in `CmakeLists.txt` in the line `project(lomiri-weather-app VERSION X.Y.Z LANGUAGES CXX)`
- `git add CMakeLists.txt`
- `git commit`
    - this Commit message is used as Changelog in OpenStore
    - use without -m, to get a window for multiline commit messages
    - first line: version in the format vX.Y.Z [obligatory, otherwise changelog in OpenStore does not show a version number], use [sematinc versioning](https://semver.org/) if possible
    - next lines: Changelog
    - if „- fix #51“ is used in commit messages, the named issue is closed automatically
    - save with CRTL + O, exit with CRTL + X
- `git tag vX.Y.Z`
- `git push && git push --tags`
    - pushes the commit with manifest and the tag into the online repo
    - triggers the gitlab CI to creates the click file with the version from the tag
    - once the build is succesfull, the new version is automatically released to OpenStore
- edit the new tag online and add the changelog text as description, then this automaticaly creates a release under the repo‘s subpage project/releases
- update screenshots if needed and ask Brian to upload them to OpenStore

# Troubleshooting

## Issues in the released version

Alwasy install the app from OpenStore after releasing and test it. In case of problems with this new version, apply fixes and re-release as described above.

## Build failing

In case the build fails, there are three options:

1. push a new tag
       `git tag vX.Y.Zb`
       `git push && git push --tags`
       As long as there is no new commit, the commitmessage of the last commit is still used.
2. release an entire new version as described above (include changlog commit message text from the failed version, that does not get included automatically)
3. Check the build log. There might be a problem with the gitlab CI. Find the issue, fix it. Then you should be given the option to retry the build.
