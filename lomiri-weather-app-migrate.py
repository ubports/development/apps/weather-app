#!/usr/bin/python3

import hashlib
import os
import sys
from pathlib import Path

organization = "weather.ubports"
application = "weather.ubports"
old_application = "com.ubuntu.weather"

xdg_config_home = Path(os.environ.get("XDG_CONFIG_HOME",
                                      Path.home() / ".config"))
xdg_data_home = Path(
    os.environ.get("XDG_DATA_HOME", Path.home() / ".local" / "share")
)
old_config_file = xdg_config_home / organization / f"{old_application}.conf"
new_config_file = xdg_config_home / organization / f"{application}.conf"
if old_config_file.is_file() and not new_config_file.exists():
    old_config_file.rename(new_config_file)

db_file = (
    xdg_data_home / organization / "Databases" /
    f"{hashlib.md5(application.encode()).hexdigest()}.sqlite"
)
old_db_file = (
    db_file.parent /
    f"{hashlib.md5(old_application.encode()).hexdigest()}.sqlite"
)
if old_db_file.with_suffix(".ini").is_file() and not db_file.exists():
    with old_db_file.with_suffix(".ini").open() as old, \
         db_file.with_suffix(".ini").open(mode="w+") as new:
        for line in old:
            if line.strip() == f"Name={old_application}":
                line = f"Name={application}\n"
            new.write(line)
    old_db_file.with_suffix(".ini").unlink()
    old_db_file.rename(db_file)

if len(sys.argv) > 1:
    os.execvp(sys.argv[1], sys.argv[1:])
