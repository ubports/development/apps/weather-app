/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QStandardPaths>
#include <QDir>
#include <QtWebEngine>

#include <libintl.h>

#include "config.h"

int main(int argc, char *argv[])
{
    QtWebEngine::initialize();
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
    QGuiApplication *app = new QGuiApplication(argc, (char **)argv);
    app->setApplicationName("weather.ubports");
    app->setOrganizationName(app->applicationName()); // to find short settings location
    app->setApplicationVersion(projectVersion());

    std::string domainDir = localeDirectory().toStdString();

    textdomain("lomiri-weather-app");
    bindtextdomain("lomiri-weather-app", domainDir.c_str());
    bind_textdomain_codeset("lomiri-weather-app", "UTF-8");

    qDebug() << "Starting app from main.cpp";

    QQuickView *view = new QQuickView();

    // preserve old database location
    QString dataFolder = QFile::decodeName(qgetenv("XDG_DATA_HOME"));
    if (dataFolder.isEmpty())
        dataFolder = QDir::homePath() + QLatin1String("/.local/share");
    dataFolder += QDir::separator() + app->applicationName();
    view->engine()->setOfflineStoragePath(dataFolder);

    view->setSource(QUrl("qrc:/lomiri-weather-app.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
