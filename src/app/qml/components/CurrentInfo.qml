/*
 * Copyright (C) 2015 Canonical Ltd.
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1

Item {
    id: currentInfoItem
    anchors {
        horizontalCenter: parent.horizontalCenter
    }
    height: collapsedHeight
    width: weatherApp.width

    state: "normal"
    states: [
        State {
            name: "normal"
            PropertyChanges {
                target: currentInfoItem
                height: collapsedHeight
            }
            PropertyChanges {
                target: expandedInfo
                opacity: 0
            }
            PropertyChanges {
                target: updownIcon
                name: "down"
            }
        },
        State {
            name: "expanded"
            PropertyChanges {
                target: currentInfoItem
                height: expandedHeight
            }
            PropertyChanges {
                target: expandedInfo
                opacity: 1
            }
            PropertyChanges {
                target: updownIcon
                name: "up"
            }
        }
    ]
    transitions: [
        Transition {
            from: "normal"
            to: "expanded"
            SequentialAnimation {
                ScriptAction {
                    script: expandedInfo.active = true
                }
                NumberAnimation {
                    easing.type: Easing.InOutQuad
                    properties: "height,opacity"
                }
            }
        },
        Transition {
            from: "expanded"
            to: "normal"
            SequentialAnimation {
                NumberAnimation {
                    easing.type: Easing.InOutQuad
                    properties: "height,opacity"
                }
                ScriptAction {
                    script: expandedInfo.active = false
                }
            }
        }
    ]

    property int collapsedHeight  //set in LocationPane.qml at coloredBackgroundRectangle
    property int expandedHeight: collapsedHeight + units.gu(4) + (expandedInfo.item ? expandedInfo.item.height : 0)

    objectName: "currentInfo"

    property var modelData

    property alias now: nowTempLabel.text
    property alias icon: conditionIcon.source
    property alias conditionNow: descriptionLabel.text
    property alias feelsLike: feelsLabel.text
    property alias clouds: cloudsLabel.text
    property alias visibility: visibilityLabel.text
    property alias wind: windLabel.text
    property alias windGusts: gustsLabel.text
    property alias humidity: humidityLabel.text
    property alias uvIndex: uvLabel.text
    property alias precipitationPropability: precipPropLabel.text

    property color highColor: LomiriColors.orange
    property color lowColor: LomiriColors.blue

    RowLayout {
        id: conditionRow
        width: parent.width
        anchors {
            top: parent.top
            topMargin: units.gu(1.25)
        }
        Label {
            id: descriptionLabel
            Layout.alignment: Qt.AlignHCenter

            font.weight: Font.Normal
            fontSize: "large"
            width: parent.width
        }
    }

    Item {
        id: currentDataItem
        width: parent.width

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: conditionRow.bottom
            topMargin: units.gu(1.25)
        }

        Column {
            id: leftColumn
            width: parent.width/2 - units.gu(1)
            anchors {
                right: parent.horizontalCenter
                rightMargin: units.gu(1)
            }
            spacing: units.gu(1)

            Row {
                anchors.right: parent.right
                width: parent.width
                spacing: units.gu(1.5)
                layoutDirection: Qt.RightToLeft

                GridLayout {
                    width: units.gu(8)
                    rowSpacing: units.gu(0.2)
                    columnSpacing: units.gu(0.5)
                    columns: 2

                    anchors {
                        top: parent.top
                        topMargin: units.gu(0.4)
                    }

                    Icon {
                        id: highImage
                        Layout.alignment: Qt.AlignLeft
                        height: units.gu(2)
                        width: units.gu(2)
                        name: "weather-clear-symbolic"
                        color: settings.highColor
                    }

                    Label {
                        id: highLabel
                        Layout.alignment: Qt.AlignRight
                        color: settings.highColor
                        font.weight: Font.Normal
                        font.pixelSize: units.gu(2.2)
                        text: modelData.high
                    }

                    Icon {
                        id: lowImage
                        Layout.alignment: Qt.AlignLeft
                        height: units.gu(2)
                        width: units.gu(2)
                        source: "qrc:/extended-information_moon.svg"
                        color: settings.lowColor
                    }

                    Label {
                        id: lowLabel
                        Layout.alignment: Qt.AlignRight
                        color: settings.lowColor
                        font.weight: Font.Normal
                        font.pixelSize: units.gu(2.2)
                        // font.weight: Font.Light     //use this when emphasising high temp
                        // fontSize: "medium"          //use this when emphasising high temp
                        text: modelData.low
                    }

                    Rectangle {
                        id: spacer1
                        height: units.gu(0.75)
                        width: height
                        color: "transparent"
                    }

                    Rectangle {
                        id: spacer2
                        height: units.gu(0.75)
                        width: height
                        color: "transparent"
                    }

                    Icon {
                        id: popIcon
                        Layout.alignment: Qt.AlignLeft
                        color: theme.palette.normal.baseText
                        width: units.gu(2.2)
                        height: width
                        source: "qrc:/extended-information_chance-of-sleet.svg"
                    }

                    Label {
                        id: precipPropLabel
                        Layout.alignment: Qt.AlignRight
                        font.pixelSize: units.gu(2.2)
                        font.weight: Font.Normal
                    }
                }

                Label {
                    id: nowTempLabel
                    font.pixelSize: units.gu(5.5)
                    font.weight: Font.Light
                    height: units.gu(6)
                    // width: units.gu(6)
                    verticalAlignment: Text.AlignBottom  // AlignBottom seems to put it at the top?
                }
            }

            Label {
                id: feelsLabel
                anchors.right: parent.right
                font.weight: Font.Normal
                font.pixelSize: units.gu(2.2)
            }
        }

        Icon {
            id: conditionIcon

            anchors {
                left: parent.horizontalCenter
                leftMargin: units.gu(1)

                top: parent.top
                topMargin: units.gu(0.5)
            }
            width: units.gu(14)
            height: width
        }

        Item {
            id: bottomColumn
            width: parent.width
            height: childrenRect.height
            anchors {
                horizontalCenter: parent.horizontalCenter

                top: conditionIcon.bottom
                topMargin: units.gu(0.5)
            }


            Column {
                spacing: units.gu(1)
                anchors.right: centerSpacer.left
                Label {
                    id: visibilityLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
                Label {
                    id: humidityLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
                Label {
                    id: uvLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
            }

            Rectangle {
                id: centerSpacer
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(2)
                color: "transparent"
            }

            Column {
                spacing: units.gu(1)
                anchors.left: centerSpacer.right
                Label {
                    id: windLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
                Label {
                    id: gustsLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
                Label {
                    id: cloudsLabel
                    font.weight: Font.Normal
                    font.pixelSize: units.gu(1.75)
                }
            }
        }

        Icon {
            id: updownIcon
            anchors {
                horizontalCenter: parent.horizontalCenter
                top: bottomColumn.bottom
                topMargin: units.gu(0.5)
            }
            height: units.gu(2)
            width: units.gu(2)
            name: "down"
        }

        Loader {
            id: expandedInfo
            width: parent.width
            active: false
            anchors {
                top: updownIcon.bottom
                topMargin: units.gu(3)
            }
            asynchronous: true
            opacity: 0
            source: "DayDelegateExtraInfo.qml"

            property var modelData: {
                var tmp = ({});

                // Remove the condition only for today extended info modelData
                // as it is provided in current data in the column above
                if (todayData) {
                    tmp = todayData;
                    tmp.condition = "";
                }

                return tmp;
            }
        }
    }

    MouseArea {
        anchors {
            fill: parent
        }
        onClicked: {
            parent.state = parent.state === "normal" ? "expanded" : "normal"
            // -2 as this is in header (not a delegate) and needs a fake index
            locationPages.collapseOtherDelegates(-2)
        }
    }

    Behavior on height {
        NumberAnimation {
            easing.type: Easing.InOutQuad
        }
    }

    Component.onCompleted: {
        locationPages.collapseOtherDelegates.connect(function(otherIndex) {
            // -2 as this is in header (not a delegate) and needs a fake index
            if (currentInfoItem && typeof index !== "undefined" && otherIndex !== -2) {
                state = "normal"
            }
        });
    }
}
