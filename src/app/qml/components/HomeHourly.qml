/*
 * Copyright (C) 2015 Canonical Ltd.
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Lomiri.Components 1.3

ListView {
    id: homeHourly

    clip: true
    height: parent ? parent.height : undefined
    width: parent ? parent.width : undefined
    model: hourlyForecastsData.length
    orientation: ListView.Horizontal

    property string currentDate: Qt.formatTime(new Date())

    onVisibleChanged: {
        if (visible) {
            ListView.model = hourlyForecastsData.length
        }
    }

    delegate: Item {
        id: delegate

        property var hourData: hourlyForecastsData[index]
        property var itemHeight: units.gu(2.5)

        height: parent.height
        width: childrenRect.width

        Column {
            id: hourColumn

            anchors.verticalCenter: parent.verticalCenter
            height: childrenRect.height
            //some AM/PM notations take up more space than other formats, allow some extra width for those
            width: isAmPmTimeformat() ? units.gu(11.5) : units.gu(10)

            function isAmPmTimeformat() {
                var check = ["am","a.m.","AM","A.M.","pm","p.m.","PM","P.M."]
                var isAmPmFormat = false
                for (var str in check) {
                    if (getDate(hourData.date).toLocaleTimeString(Qt.locale(),Locale.ShortFormat).search(check[str]) !== -1) {
                        isAmPmFormat = true
                    }
                }
                return isAmPmFormat
            }

            Label {
                id: timestampLabel
                anchors.horizontalCenter: parent.horizontalCenter
                fontSize: "small"
                font.weight: Font.Light
                text: "%1 %2".arg(formatTimestamp(hourData.date, 'ddd')).arg(getDate(hourData.date).toLocaleTimeString(Qt.locale(),Locale.ShortFormat))
            }

            Item {
                id: iconItem
                anchors.horizontalCenter: parent.horizontalCenter
                height: units.gu(7)
                width: units.gu(7)

                Icon {
                    id: weatherIcon
                    anchors {
                        fill: parent
                        margins: units.gu(0.5)
                    }
                    name: (hourData.icon !== undefined && iconMap[hourData.icon] !== undefined) ? iconMap[hourData.icon] : ""
                }
                //daily forecast does only yield "rain" icon for all intensities of rain from small drizzle to heavy rain
                //use wrapper function to determine the rain icon based on the amount of rainfall
                //if "rain" icon is transmitted, skip "image" (system icon) and use a in-app svg graphic instead
                function getRainSnowIcon(rainVolume,snowVolume,weather) {
                    var resulticon
                    if (weather === "rain" || weather === "snow") {
                        if (rainVolume > 0 && snowVolume < rainVolume/10) {
                              //see https://de.wikipedia.org/wiki/Regen#Regenformen for classification
                              //rain volume in mm per hour
                              if (rainVolume < 0.5) { //light rain
                                  resulticon = "qrc:/weather-1drop-symbolic.svg";
                              } else if (rainVolume >= 0.5 && rainVolume < 4) { //medium rain
                                  resulticon = "qrc:/weather-2drops-symbolic.svg";
                              } else if (rainVolume >= 4 && rainVolume < 10) { //rain
                                  resulticon = "qrc:/weather-3drops-small-symbolic.svg";
                              } else if (rainVolume >= 10 && rainVolume < 20) { //heavy rain
                                  resulticon = "qrc:/weather-3drops-big-symbolic.svg";
                              } else if (rainVolume >= 20) { //very heavy rain
                                  resulticon = "qrc:/weather-4drops-symbolic.svg";
                              }
                              weatherIcon.source = resulticon;
                        } else if (snowVolume > 0 && rainVolume < snowVolume/10) {
                              //categories based on rainfall volume, see above
                              //snow volume in mm per hour
                              if (snowVolume < 0.5) { //light snow
                                  resulticon = "qrc:/weather-1flake-symbolic.svg";
                              } else if (snowVolume >= 0.5 && snowVolume < 4) { //medium snow
                                  resulticon = "qrc:/weather-2flakes-symbolic.svg";
                              } else if (snowVolume >= 4 && snowVolume < 10) { //snow
                                  resulticon = "qrc:/weather-3flakes-small-symbolic.svg"; //TODO: create icon
                              } else if (snowVolume >= 10 && snowVolume < 20) { //heavy snow
                                  resulticon = "qrc:/weather-3flakes-big-symbolic.svg"; //TODO: create icon
                              } else if (snowVolume >= 20) { //very heavy snow
                                  resulticon = "qrc:/weather-4flakes-symbolic.svg";
                              }
                              weatherIcon.source = resulticon;
                        } else if (rainVolume >= snowVolume/5 && snowVolume >= rainVolume/5) {
                            var combined = rainVolume + snowVolume
                            if (combined == 0) {
                                // sometimes there is a chance for rain/snow and icon "rain" send, but forecast volume is zero
                                // show icon for light rain/light snow
                                if (weather === "rain") {
                                    resulticon = "qrc:/weather-1drop-symbolic.svg";
                                } else if (weather === "snow") {
                                    resulticon = "qrc:/weather-1flake-symbolic.svg";
                                } else {
                                    // not sure if there can be another case, show sleet icon then
                                    resulticon = "qrc:/weather-sleet1-symbolic.svg";
                                }
                            } else if (combined > 0 && combined < 1) {
                                resulticon = "qrc:/weather-sleet1-symbolic.svg";
                            } else if (combined >= 1 && combined < 15) {
                                resulticon = "qrc:/weather-sleet2-symbolic.svg";
                            } else if (combined >= 15) {
                                resulticon = "qrc:/weather-sleet3-symbolic.svg";
                            }
                            weatherIcon.source = resulticon;
                        } else {
                            resulticon = ""; //TODO: add mixed downfall icons, temporary use sleet icon for all
                        }
                    }
                    return resulticon;
                }
                Component.onCompleted: getRainSnowIcon(hourData["metric"].rain, hourData["metric"].snow, hourData.icon)
            }

            Label {
                id: tempLabel
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: units.gu(3)
                font.weight: Font.Light
                text: getTemp(hourData["metric"].temp, true)
            }

            Item {
                id: chanceOfPrecipRow
                width: childrenRect.width
                height: itemHeight
                anchors.horizontalCenter: parent.horizontalCenter
                visible: popLabel.text !== ""
                Icon {
                    id: popIcon
                    anchors.verticalCenter: parent.verticalCenter
                    color: theme.palette.normal.baseText
                    width: units.gu(2)
                    height: width
                    source: "qrc:/extended-information_chance-of-sleet.svg"
                }
                Label {
                    id: popLabel
                    anchors.left: popIcon.right
                    anchors.leftMargin: units.gu(0.5)
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: units.gu(2)
                    font.weight: Font.Light
                    text: hourData.pop !== undefined ? i18n.ctr("%1 will be replaced with the value, unit % for chance of precipitation, hourly forecast", "%1%").arg(hourData.pop*100) : ""
                }
            }

            Label {
                id: snowrainLabel
                height: itemHeight
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.rightMargin: units.gu(2)
                font.pixelSize: units.gu(2)
                font.weight: Font.Light
                // shows the combined value of total downfall rain + snow
                text: getCombinedRainSnowValue()

                function getCombinedRainSnowValue() {
                    // 1. get snow value, 2. get rain value
                    // combine values with rain unit if rain is greater than snow or both are equal
                    // combine values with snow unit if snow is greater than rain and replace percentage icon with "chance of snow" icon
                    // add unit according to setting
                    // put in decimal separator according to set locale

                    var rainVolume = hourData["metric"].rain + hourData["metric"].showers // test if undefined
                    var snowVolume = hourData["metric"].snow // test if undefined
                    var label = ""
                    var value = rainVolume + snowVolume

                    if (rainVolume == 0 && snowVolume == 0) {
                        label = getRainLabel(value)
                    } else if (rainVolume > 0 && snowVolume < rainVolume/50) {
                        label = getRainLabel(value)
                    } else if (snowVolume > 0 && rainVolume < snowVolume/50) {
                        label = getSnowLabel(value)
                        popIcon.source = "qrc:/extended-information_chance-of-snow.svg"
                    } else {
                        label = getRainLabel(value)
                        popIcon.source = "qrc:/extended-information_chance-of-sleet.svg"
                    }

                    return label
                }
            }

            Item {
                id: sunshineDurationRow
                width: childrenRect.width
                height: itemHeight
                anchors.horizontalCenter: parent.horizontalCenter
                Icon {
                    id: sunIcon
                    anchors.verticalCenter: parent.verticalCenter
                    color: theme.palette.normal.baseText
                    width: units.gu(2)
                    height: width
                    name: sunshineLabel.text === "" ? "weather-few-clouds-night-symbolic" : "weather-clear-symbolic"
                }
                Label {
                    id: sunshineLabel
                    anchors.left: sunIcon.right
                    anchors.leftMargin: units.gu(0.5)
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: units.gu(2)
                    font.weight: Font.Light
                    text: hourData.sunshinetime
                }
            }

            Label {
                id: windLabel
                height: itemHeight
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: units.gu(2)
                font.weight: Font.Light
                text: getWindSpeed(hourData.windSpeed)
            }
        }

        Rectangle {
            anchors.verticalCenter: parent.verticalCenter
            color: LomiriColors.darkGrey
            height: hourColumn.height
            opacity: 0.2
            visible: index > 0
            width: units.gu(0.1)
        }

        function getRainLabel(value) {
            var label = ""
            if (settings.precipUnit == "in") {
                //TRANSLATORS: rain volume in inch, keep/remove the space before the unit according to your language specifications
                value = Math.round(calcInch(value)*100)/100
                label = i18n.tr("%1 in").arg(localeDezimalSeparator(value.toFixed(2).replace("0.00","0")))
            } else if (settings.precipUnit == "l") {
                //TRANSLATORS: rain volume in litre per squaremeter, keep/remove the space before the unit according to your language specifications
                label = i18n.tr("%1 l/m²").arg(localeDezimalSeparator(value.toFixed(1).replace("0.0","0")))
            } else {
                //TRANSLATORS: rain volume in millimeter, keep/remove the space before the unit according to your language specifications
                label = i18n.tr("%1 mm").arg(localeDezimalSeparator(value.toFixed(1).replace("0.0","0")))
            }
            return label
        }

        function getSnowLabel(value) {
            var label = ""
            if (settings.snowUnit == "in") {
                //TRANSLATORS: snow volume in inch, keep/remove the space before the unit according to your language specifications
                value = Math.round(calcInch(value)*100)/100
                label = i18n.tr("%1 in").arg(localeDezimalSeparator(value.toFixed(2).replace("0.00","0")))
            } else if (settings.snowUnit == "mm") {
                //TRANSLATORS: snow volume in millimeter, keep/remove the space before the unit according to your language specifications
                label = i18n.tr("%1 mm").arg(localeDezimalSeparator(value.toFixed(1).replace("0.0","0")))
            } else {
                //TRANSLATORS: snow volume in centimeter, keep/remove the space before the unit according to your language specifications
                label = i18n.tr("%1 cm").arg(localeDezimalSeparator(value.toFixed(1).replace("0.0","0")))
            }
            return label
        }
    }
}
