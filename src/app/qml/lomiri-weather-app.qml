/*
 * Copyright (C) 2015 Canonical Ltd.
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Qt.labs.settings 1.0
import Lomiri.Components 1.3
import Lomiri.Connectivity 1.0
import QtPositioning 5.2
import "components"
import "data" as Data
import "data/WeatherApi.js" as WeatherApi
import LomiriWeather 1.0
import "ui"

MainView {
    id: weatherApp

    objectName: "weather"

    /*
    When the app is started with clickable desktop, the check for network connection via Connectivity does not work.
    If the app is run in desktop mode, set this property to 'true' and the check will be disabled.
    */
    property bool isDesktopMode: false

    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    backgroundColor: theme.palette.normal.background

    anchorToKeyboard: true

    signal listItemSwiping(int i)
    signal themeChanged()

    /*
      List of locations and their data, accessible through index
    */
    property var locationsList: []

    /*
      Index of Location before a refresh, to go back after
    */
    property int indexAtRefresh: -1

    /*
      Is the app loading?
    */
    property bool loading: false

    /*
      Indicates if the last API call resulted in a network error
      see https://ubports.gitlab.io/docs/api-docs/index.html?p=connectivity%2Fqml-lomiri-connectivity-networkingstatus.html
    */
    property bool networkError: isDesktopMode ? false : Connectivity.online ? false : true // in normal usage derived from Connectivity, only set false for desktop mode

    Connections {
       target: Connectivity
       onOnlineChanged: {
         networkError = isDesktopMode ? false : Connectivity.online ? false : true
         loading = Connectivity.online ? loading : false
         if (Connectivity.online) { refreshData(false,true); }
       }
    }

    /*
      Indicates if the location info was requested by Url
    */
    property var requestLocationByUrl;

    // TODO: make this a proper model, don't request data from JS
    Connections {
        target: Provider

        /**
         * Emitted per location with provider data
         *
         * @param coordinate {QGeoCoordinate} coordinate from the provider
         * @param current {DataPoint} current weather
         * @param forecast {DataDay[]} forecast data
         * @param userData user data passed to the request for this location
         */
        onDataReceived: {
            //console.log("data received", JSON.stringify({ userData, current, forecast: forecast[0] }));
            // TODO: temporary to make the switch easier, data should be more normalized

            let locationIndex = locationsList.findIndex((loc) => loc.db.id === userData.db.id);
            if (locationIndex < 0) {
                locationIndex = locationsList.length;
            }
            const location = Object.assign({}, locationsList[locationIndex] || userData);

            // with Provider all data points can be undefined to accomodate different providers
            // use -255 since 0 can look correct
            const checkUndefined = (variable) => variable !== undefined ? variable : -255;
            const breakDate = (date) => ({
                year: date.getFullYear(),
                month: date.getMonth(),
                date: date.getDate(),
                hours: date.getHours(),
                minutes: date.getMinutes(),
            });

            const currentMapped = {
                date: breakDate(current.timestamp),
                timestamp: current.timestamp.getTime() / 1000,
                metric: {
                    temp: checkUndefined(current.temperature),
                    feels_like: checkUndefined(current.feelsLike),
                },
                humidity: checkUndefined(current.humidity),
                pressure: checkUndefined(current.pressure),
                windSpeed: checkUndefined(current.windSpeed),
                windDeg: checkUndefined(current.windAzimuth),
                windDir: current.windDirectionString,
                windGusts: checkUndefined(current.windGusts),
                clouds: checkUndefined(current.cloudCover),
                visibility: checkUndefined(current.visibility),
                icon: current.icon,
                uvIndex: checkUndefined(current.uvIndex.toFixed(2)),
                precipProp: checkUndefined(current.precipitationProbability),
                condition: current.description || "",
                coordLAT: coordinate.latitude,
                coordLON: coordinate.longitude,
                service: "openmeteo",
                service_id: `${coordinate.latitude}x${coordinate.longitude}`,
            };

            location.data = forecast.map((day, i) => ({
                date: breakDate(day.data.timestamp),
                timestamp: day.data.timestamp.getTime() / 1000,
                metric: {
                    tempMin: checkUndefined(day.data.temperatureMin),
                    tempMax: checkUndefined(day.data.temperatureMax),
                },
                pop: checkUndefined(day.data.precipitationProbability) / 100,
                rain: checkUndefined(day.data.rain) + checkUndefined(day.data.showers),
                snow: checkUndefined(day.data.snowfall),
                pressure: checkUndefined(day.data.pressure).toFixed(0),
                uvIndexDay: checkUndefined(day.data.uvIndexDay).toFixed(2),
                humidity: checkUndefined(day.data.humidity).toFixed(0),
                sunshinetime: Qt.formatTime(new Date(0, 0, 0, 0, 0, day.data.sunshineDuration), "hh:mm"),
                icon: day.data.icon,
                condition: day.data.description || "",
                windSpeed: checkUndefined(day.data.windSpeed),
                windDeg: checkUndefined(day.data.windAzimuth),
                windDir: day.data.windDirectionString,
                hourly: day.hourly
                    // filter hourly before now - 1h for compatibility
                    .filter((hourly) => hourly.timestamp >= Date.now() - 1000 * 60 * 60)
                    .map((hourly) => ({
                        date: breakDate(hourly.timestamp),
                        timestamp: hourly.timestamp.getTime() / 1000,
                        metric: {
                            temp: checkUndefined(hourly.temperature),
                            rain: checkUndefined(hourly.rain),
                            showers: checkUndefined(hourly.showers),
                            snow: checkUndefined(hourly.snowfall),
                        },
                        pop: checkUndefined(hourly.precipitationProbability) / 100,
                        icon: hourly.icon,
                        condition: hourly.description || "",
                        windSpeed: checkUndefined(hourly.windSpeed),
                        sunshinetime: hourly.isDay || hourly.sunshineDuration > 0 ? Qt.formatTime(new Date(0, 0, 0, 0, 0, hourly.sunshineDuration), "h:mm") : "",
                    })),
                current: i === 0 ? currentMapped : undefined,
            }));

            location.save = true;
            location.updated = Date.now();
            storage.updateLocation(location.db.id, location);

            locationsList.splice(locationIndex, 1, location);
            fillPages(locationsList);
        }

        /**
         * Emitted on error in provider
         *
         * @param message {string} error message
         * @param data various error data
         */
        onError: {
            networkError = true;
            loading = false;

            console.error("error getting weather data", message, data);
        }
    }

    /*
      (re)load the pages on completion
    */
    Component.onCompleted: {
        // TODO: read from settings once any other provider exists
        Provider.provider = OpenMeteoWeatherProvider;

        var url = args.defaultArgument.at(0)
        var argus = url ? parseArguments(url) : {};

        mainPageStack.push(Qt.resolvedUrl("ui/HomePage.qml"))

        //specify same/similar methods in UriHandler section
        //here links get processed on app startup = when received with app closed
        storage.getLocations(fillPages);
        if (argus.city && argus.lat && argus.lng) {
            console.log("link received while app is closed")
            if (positionViewAtCity(argus.city) === -1) {
                searchForLocation("searchByUrl", argus.lat, argus.lng)
            }
        } else if (argus.location){
            console.log("link received while app is closed")
            // run search by name
            if (positionViewAtCity(argus.location) === -1) {
                var  parameters = {
                    name: argus.location,
                    units: "metric",
                    lang: getLocale()
                }
                //retrieve search results
                WeatherApi.sendRequest({
                                           action: "searchByName",
                                           params: parameters,
                                       }, searchResponseHandler)
                //new location gets added at the last position -> set that as current index
                // settings.current =  locationsList.length-1
                // loading = false
            }


        } else {
            refreshData(false,true);  //force refresh when app is opened
        }

        setCurrentTheme();
        refreshDataTimer.start()
    }

    Settings {
        id: settings
        category: "weatherSettings"
        /*
          Index of the current locationList of locations and their data, accessible through index
        */
        property int current: 0

        property bool detectCurrentLocation: false
        property int refreshInterval: 1800 //equals to 30 minutes as default interval
        property string precipUnit
        property string snowUnit
        property string service
        property string tempScale
        property string windUnit
        property string distanceUnit

        property bool addedCurrentLocation: false
        property bool migrated: false

        property string selectedTheme: "System"
        property string colorTheme: "no colors"
        property color highColor: LomiriColors.orange
        property color lowColor: theme.palette.selected.focus
        property bool highlightToday: false

        //remove current location if one is set and the 'Detect current location' setting get's turned off
        onDetectCurrentLocationChanged: {
            if (!detectCurrentLocation) {
                if (addedCurrentLocation) {
                    storage.removeLocation(-1);  // indexes are decreased by 1
                    addedCurrentLocation = false;
                    console.log("removed GPS location")
                }

                refreshData();
            }
        }

        Component.onCompleted: {
            //make sure settings are filled with valid values, if not use defaults
            if (!["openmeteo"].includes(service)) {
                //wrong provider set -> use default
                console.log("None or invalid data provider was set. Setting default: openmeteo. Value is: " + service)
                service = "openmeteo"
            }
            if (!["m/s","km/h","mph"].includes(windUnit)) {
                //wrong wind speed unit set -> use default
                console.log("None or invalid wind speed unit was set. Setting default: m/s. Value is: " + windUnit)
                windUnit = "m/s"
            }
            if (!["mm","in","l"].includes(precipUnit)) {
                //wrong downfall unit set -> use default
                console.log("None or invalid downfall unit was set. Setting default: mm. Value is: " + precipUnit)
                precipUnit = "mm"
            }
            if (!["mm","in","cm"].includes(snowUnit)) {
                //wrong snow unit set -> use default
                console.log("None or invalid snow unit was set. Setting default: cm. Value is: " + snowUnit)
                snowUnit = "cm"
            }
            if (!["°C","°F"].includes(tempScale)) {
                //wrong temperature unit set -> use default
                console.log("None or invalid temperature unit was set. Setting default: °C. Value is: " + tempScale)
                tempScale = "°C"
            }

            setTemperatureColors(colorTheme)
            setCurrentTheme()
        }
    }

    /* Fill the location pages with their data. */
    function fillPages(locations) {
        locationsList = []
        locationsList = locations;

        // Loading is complete
        // Either directly from cached entries being passed here
        //  - storage.getLocations(fillPages);
        // or after updateData has occurred
        //  - fillPages(locationsList) from Provider.onDataReceived
        loading = false;
    }

    /*
      Refresh data, either directly from storage or by checking against
      API instead.
    */
    function refreshData(fromStorage = false, forceRefresh = false) {
        networkError = !isDesktopMode && !Connectivity.online;
        loading = !networkError;

        if ((fromStorage && !forceRefresh) || networkError) {
            storage.getLocations(fillPages);
        } else {
            storage.getLocations((locations) => {
                const filtered = locations
                    .filter((loc) => forceRefresh || !loc.updated || (Date.now() - loc.updated > settings.refreshInterval * 1000));
                const coords = filtered
                    .map((loc) => QtPositioning.coordinate(loc.location.coord.lat, loc.location.coord.lon));
                Provider.getData(coords, filtered);
            });
        }
    }

    Timer {
        //refreshes the data at the interval choosen in settings
        //only works while the app is open, may need background suspension disabled
        id: refreshDataTimer
        interval: settings.refreshInterval*1000; running: false; repeat: true;
        onTriggered: refreshData(false, true)
    }

    /*
      parse arguments retrieved from url. The url format can be as following:
      http://openweathermap.org/find?q=london
      weather://?city=London&lat=51.50853&lng=-0.12574
    */
    function parseArguments(url) {
        //determine which type of url does get submitted
        //can be weather OR https OR http
        //return substring from start to first ":"
        var sender = url.substring(0,url.lastIndexOf(':'))

        if (sender === "weather") {
            //weather format does provide a cityname + lat and long coords
            //example: weather://?city=London&lat=51.50853&lng=-0.12574
            //return substring from "?" until end of string
            var arguments = url.substring(url.lastIndexOf('?') + 1).split('&');
            var city, lat, lng;
            var index = 0;
            while(index < arguments.length) {
                var params = arguments[index].split('=');
                if (params[0] === "lng")
                    lng = params[1];
                if (params[0] === "lat")
                    lat = params[1];
                if (params[0] === "city")
                    city = params[1];

                index++;
            }
            return {"city": city, "lat":  lat, "lng" : lng}

        } else if (sender === "https" || sender === "http") {
            //http + https format does provide a OpenWeatherMap city search string
            //example (London): https://openweathermap.org/find?q=london
            //return substring from "=" until end of string
            var location = url.substring(url.lastIndexOf('=') + 1);
            return {"location": location}

        } else  return {"location": undefined,"city": undefined, "lat":  undefined, "lng" : undefined}
    }

    /*
      Returns the index of the city if it can be found in locationlist
      and return -1 if city is not found in locationlist.
      Used e.g. to position the view at the city specified.
      NOTE: potential minor issue: cities with the same name in different countries
    */
    function positionViewAtCity(cityName) {
        var index = -1;

        for (var i = 0; i < locationsList.length; i++) {
            var loc = locationsList[i].location;
            if (cityName === loc.name) {
                settings.current =  i;
                index  = i;
                break;
            }
        }

        return index;
    }

    // called with "searchByUrl" from URI handler and on componentCompleted
    // called with "searchByPoint" only from currentLocation component
    // NOT called with "searchByName" when a new location is added via name search
    function searchForLocation(action, lat, lon) {
        WeatherApi.sendRequest({
            action: action,
            params: {
                coords: {
                    lat: lat,
                    lon: lon
                },
                //used to retrieve the city name translated in locale
                lang: getLocale()
            }
        }, searchResponseHandler)
    }

    // only called in function searchForLocation above
    function searchResponseHandler(msgObject) {
        if (!msgObject.error ) {
            console.log("Location to add:", JSON.stringify(msgObject.result.locations[0]))
            if (msgObject.action === "searchByUrl") {
                // calls from UriHandler (weather) or on componentCompleted
                requestLocationByUrl = msgObject.result.locations[0];
                storage.addLocation(requestLocationByUrl)
            } else if (settings.detectCurrentLocation) {
                // calls from search function of GeocodeModel in currentLocation component => for GPS locations
                storage.updateCurrentLocation(msgObject.result.locations[0])
            } else if (msgObject.action === "searchByName") {
                //calls from UriHandler (https/http)
                requestLocationByUrl = msgObject.result.locations[0];
                storage.addLocation(requestLocationByUrl)
            }
        }
    }

    /*
    Stanard format for Qt.locale is like en_GB, see https://doc.qt.io/qt-5.12/qml-qtqml-locale.html#name-prop
    OWM does not use this standard international locale but has special syntax, see https://openweathermap.org/current#multi
    - most languages only two first locale letters
    - a few languages with the full number of letters, but lowercase
    - those languages do not follow a pattern, so need to be hardcoded
    This function returns either standard locale or OWM hardcoded locale.
    */
    function getLocale() {
        var locale = Qt.locale().name
        if (settings.service === "openmeteo") {
            if (locale === "pt_BR" || locale === "zh_CN" || locale === "zh_TW") {
                locale = locale.toLowerCase()
            } else {
                locale = locale.toLowerCase().substr(0, 2)
            }
        }
        return locale
    }

    // for troubleshooting purposes to test if cpp data succesfully reaches the qml side
    // use like this: current.cloudCover -> logValue("current cloud cover: ", current.cloudCover)
    function logValue(message, value) {
        console.log(message + ": " + value)
        return value
    }

    Arguments {
        id: args;
        //TRANSLATORS: %1 does specify the parameters, %2 does specify the url for weather apps repo at gitlab
        defaultArgument.help: i18n.tr("Valid arguments for weather app are: %1 They will be managed by system. See the README at %2 for a full comment about them").arg("--location, --city, --lat, --lng").arg("https://gitlab.com/ubports/development/apps/lomiri-weather-app/");
        defaultArgument.valueNames: []

        /* ARGUMENTS for URL dispatcher to import locations based on shared URL
        *
        * Location information. This enable us to navigate relevant city weather view with geo information specified
        * please pass all three basic geo parameters together when requesting specific city weather
        *
        * Keyword: city
        * Value: city name
        *
        * Keyword: lat
        * Value:  latitude of city
        *
        * Keyword: lng
        * Value:  longitude of city
        */

        Argument {
            name: "location"
            required: false
            valueNames: ["LOCATION"]
        }

        Argument {
            name: "city"
            required: false
            valueNames: ["CITY"]
        }

        Argument {
            name: "lat"
            required: false
            valueNames: ["LATITUDE"]
        }

        Argument {
            name: "lng"
            required: false
            valueNames: ["LONGITUDE"]
        }
    }

    Connections {
        //specify same/similar methods in Component.onCompleted section
        //here links get processed when received while app is open
        target: UriHandler
        onOpened: {
            console.log("link received while app is open")
            var argus = parseArguments(uris[0]);
            if (argus.city && argus.lat && argus.lng) {
                while (mainPageStack.depth > 1) {
                    mainPageStack.pop()
                }
                //run search by coords
                if (positionViewAtCity(argus.city) === -1) {
                    searchForLocation("searchByUrl", argus.lat, argus.lng)
                }
            }
            if (argus.location) {
                while (mainPageStack.depth > 1) {
                    mainPageStack.pop()
                }
                storage.addLocation(argus.location)
            }
        }
    }

    CurrentLocation {
        id: currentLocation
    }

    Data.Storage {
        id: storage

        // Add or replace the current location to the storage and refresh the locationList
        function updateCurrentLocation(location) {
            if (!settings.addedCurrentLocation || locationsList == null || locationsList.length == 0) {
                // if currentlocation has not been added OR locationlist is empty OR locationlist has zero entries add current location
                storage.insertLocationAtStart({location: location});
                settings.addedCurrentLocation = true;
            } else {
                // else update stored current location
                storage.updateLocation(locationsList[0].db.id, {location: location});
            }

            refreshData(false, true);
        }

        // Add the location to the storage and refresh the locationList
        // Return true if a location is added
        function addLocation(location) {
            var exists = checkLocationExists(location)

            if(!exists) {
                if(location.dbId === undefined || location.dbId === 0) {
                    storage.insertLocation({location: location});
                }
            }
            refreshData(false, true);

            return !exists;
        }

        // Return true if the location given is already in the locationsList, and is not the same
        // as the current location.
        function checkLocationExists(location) {
            var exists = false;

            for (var i=0; !exists && i < locationsList.length; i++) {
                var loc = locationsList[i].location;

                if (loc.services.geonames && (loc.services.geonames === location.services.geonames) && !(settings.addedCurrentLocation && i === 0)) {
                    exists = true;
                }
            }

            return exists;
        }

        function moveLocation(from, to) {
            // Indexes are offset by 1 to account for current location
            if (settings.addedCurrentLocation) {
                from += 1
                to += 1
            }

            // Update settings to respect new changes
            if (from === settings.current) {
                settings.current = to;
            } else if (from < settings.current && to >= settings.current) {
                settings.current -= 1;
            } else if (from > settings.current && to <= settings.current) {
                settings.current += 1;
            }

            storage.reorder(locationsList[from].db.id, locationsList[to].db.id);

            refreshData(true, false);
        }

        // Remove a location from the list
        function removeLocation(index) {
            // Indexes are offset by 1 to account for current location
            if (settings.addedCurrentLocation) {
                index += 1
            }

            if (settings.current >= index) {  // Update settings to respect new changes
                settings.current -= settings.current;
            }

            storage.clearLocation(locationsList[index].db.id);

            refreshData(true, false);
        }

        function removeMultiLocations(indexes) {
            var i;

            // Sort the item indexes as loops below assume *numeric* sort
            indexes.sort(function(a,b) { return a - b })

            for (i=0; i < indexes.length; i++) {
                if (settings.current >= indexes[i] + 1) {  // Update settings to respect new changes
                    settings.current -= settings.current;
                }
            }

            // Get location db ids to remove
            var locations = []

            for (i=0; i < indexes.length; i++) {
                if (settings.addedCurrentLocation) {
                    locations.push(locationsList[indexes[i] + 1].db.id)
                } else {
                    locations.push(locationsList[indexes[i]].db.id)
                }
            }

            storage.clearMultiLocation(locations);

            refreshData(true, false);
        }
    }

    /*
      Sets the colors for low and high temperatures according
      to the color theme that has been selected under settings.
    */
    function setTemperatureColors(colorTheme) {
        if (colorTheme == "no colors") {
          settings.highColor = theme.palette.normal.foregroundText;
          settings.lowColor = theme.palette.normal.foregroundText;
          settings.colorTheme = "no colors"
        }
        if (colorTheme == "blue/orange") {
          settings.highColor = LomiriColors.orange;
          settings.lowColor = theme.palette.selected.focus;
          settings.colorTheme = "blue/orange"
        }
        if (colorTheme == "blue/red") {
          settings.highColor = theme.palette.normal.negative;
          settings.lowColor = theme.palette.selected.focus;
          settings.colorTheme = "blue/red"
        }
        if (colorTheme == "blue/yellow") {
          settings.highColor = "#FFD42A"; //yellow
          settings.lowColor = theme.palette.selected.focus;
          settings.colorTheme = "blue/yellow"
        }
    }

    /*
      Sets the system theme according to the theme selected
      under settings.
    */
    function setCurrentTheme() {
        if (settings.selectedTheme == "System") {
          theme.name = "";
          // setTemperatureColors(settings.colorTheme);
        } else if (settings.selectedTheme == "SuruDark") {
          theme.name = "Lomiri.Components.Themes.SuruDark"
          // setTemperatureColors(settings.colorTheme);
        } else if (settings.selectedTheme == "Ambiance") {
          theme.name = "Lomiri.Components.Themes.Ambiance"
          // setTemperatureColors(settings.colorTheme);
        } else {
          theme.name = "";
          // setTemperatureColors(settings.colorTheme);
        }
        setTemperatureColors(settings.colorTheme);
    }

    /*
      Returns a string value with the common decimal separator . being replaced with the locales decimal separator.
    */
    function localeDezimalSeparator(value) {
        return value.toString().replace(".",Qt.locale().decimalPoint)
    }

    onThemeChanged: setCurrentTheme()

    PageStack {
        id: mainPageStack
    }
}
