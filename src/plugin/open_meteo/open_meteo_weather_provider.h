/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROVIDER_OPEN_METEO_H
#define PROVIDER_OPEN_METEO_H

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QUrlQuery>
#include <flatbuffers/flatbuffers.h>
#include <open_meteo/weather_api_generated.h>
#include <plugin/weather_data_provider.h>

class OpenMeteoWeatherProvider : public QObject, public WeatherDataProviderInterface
{
    Q_OBJECT
    Q_INTERFACES(WeatherDataProviderInterface)
public:
    OpenMeteoWeatherProvider();

    void getData(QList<QGeoCoordinate> locations, QVariantList userData);

signals:
    void dataReceived(QGeoCoordinate coordinate, DataPoint current, QList<DataDay> forecast,
                      QVariant userData);
    void error(QString message, QVariant data);

private:
    QNetworkAccessManager m_networkAccessManager;

    void onResponse(QNetworkReply *reply);
    void handleResponseChunk(QByteArray bytes, QVariant userData);
};

#endif // PROVIDER_OPEN_METEO_H
