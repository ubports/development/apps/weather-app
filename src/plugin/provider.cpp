/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGeoCoordinate>

#include "provider.h"
#include "weather_data_provider.h"

Provider::Provider() { }

bool Provider::getData(QVariantList locations, QVariantList userData)
{
    WeatherDataProviderInterface *iface =
            qobject_cast<WeatherDataProviderInterface *>(m_dataProvider);
    if (iface == nullptr) {
        qDebug() << "m_dataProvider is not a WeatherDataProviderInterface";
        return false;
    }

    if (locations.size() == 0) {
        return false;
    }

    QList<QGeoCoordinate> converted;
    for (const auto &variant : locations) {
        if (!variant.canConvert<QGeoCoordinate>()) {
            qDebug() << "passed location is not a QGeoCoordinate!";
            return false;
        }
        converted.append(variant.value<QGeoCoordinate>());
    }
    iface->getData(converted, userData);
    return true;
}

void Provider::setProvider(QObject *provider)
{
    WeatherDataProviderInterface *iface = qobject_cast<WeatherDataProviderInterface *>(provider);

    if (iface == nullptr) {
        qDebug() << "provider is not a WeatherDataProviderInterface";
        return;
    }

    if (m_dataProvider != nullptr) {
        disconnect(m_dataProvider, 0, this, 0);
    }

    m_dataProvider = provider;

    connect(m_dataProvider,
            SIGNAL(dataReceived(QGeoCoordinate, DataPoint, QList<DataDay>, QVariant)), this,
            SLOT(onDataReceived(QGeoCoordinate, DataPoint, QList<DataDay>, QVariant)));

    connect(m_dataProvider, SIGNAL(error(QString, QVariant)), this,
            SLOT(onError(QString, QVariant)));
}

void Provider::onDataReceived(QGeoCoordinate coordinate, DataPoint current, QList<DataDay> forecast,
                              QVariant userData)
{
    QVariantList variants;
    variants.reserve(forecast.size());
    for (auto &day : forecast) {
        variants.append(QVariant::fromValue(day));
    }

    emit dataReceived(coordinate, current, variants, userData);
}

void Provider::onError(QString message, QVariant data)
{
    emit error(message, data);
}
