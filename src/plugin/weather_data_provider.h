/*
 * Copyright (C) 2024 Maciej Sopyło <me@klh.io>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This file is part of Lomiri Weather App
 *
 * Lomiri Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WEATHER_DATA_PROVIDER_H
#define WEATHER_DATA_PROVIDER_H

#include "data_day.h"

class WeatherDataProviderInterface
{
public:
    virtual ~WeatherDataProviderInterface() = default;

    virtual void getData(QList<QGeoCoordinate> locations, QVariantList userData) = 0;

signals:
    virtual void dataReceived(QGeoCoordinate coordinate, DataPoint current, QList<DataDay> forecast,
                              QVariant userData) = 0;
    virtual void error(QString message, QVariant data) = 0;
};

Q_DECLARE_INTERFACE(WeatherDataProviderInterface, "ubports.weather.WeatherDataProviderInterface")

#endif // WEATHER_DATA_PROVIDER_H
